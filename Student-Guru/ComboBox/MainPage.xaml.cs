﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace StudentGuruDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            String direction;
            ComboBox _combobox;
            ComboBoxItem _comboboxItem;

            String semester;
            ComboBox _sCombobox;
            ComboBoxItem _scomboboxItem;

            _sCombobox = mySemesterComboBox;
            _scomboboxItem = (ComboBoxItem)_sCombobox.SelectedItem;
            semester = _scomboboxItem.Content as String;

            _combobox = myCombobox;
            _comboboxItem = (ComboBoxItem)_combobox.SelectedItem;
            direction = _comboboxItem.Content as String;

            List<String> myCourses;
            myCourses = getCourses(direction, semester);
            this.DataContext = myCourses;

        }

        public List<String> getCourses(String direction, String semester)
        {
            List<String> myList = new List<String>();
            if (direction == "Μηχανικός Λογισμικού")
            {
                if (semester == "5")
                {
                    myList.Add("Τεχνιτή Νοημοσύνη");
                    myList.Add("Ανάλυση Αλγορύθμων");
                }
                else if (semester == "6")
                {
                    myList.Add("Μηχανική Μάθηση");
                    myList.Add("Προγραμματισμός Διαδικτύου");

                }
                else  // (semester.Equals("7"))
                {
                    myList.Add("Προγραμματισμός Assembly");
                    myList.Add("Λογική στη πληροφορική");
                }
            }
            else if (direction == "Μηχανικός Δικτύου")
            {

                if (semester == "5")
                {
                    myList.Add("Δίκτυα ΙΙ");
                }
                else if (semester == "6")
                {
                    myList.Add("Θεωρία Πληροφορίας");

                }
                else  // (semester.Equals("7"))
                {
                    myList.Add("Ασύρματη επικοινωνία");
                }
            }
            else if (direction == "Μηχανικός Η/Υ")
            {

                if (semester == "5")
                {
                    myList.Add("Ενσωματωμένα συστήματα");
                }
                else if (semester == "6")
                {
                    myList.Add("Επεξεργασία Εικόνας");

                }
                else  // (semester.Equals("7"))
                {
                    myList.Add("Παράλληλα συστήματα");
                }
            }
            return myList;

        }
    }
}
